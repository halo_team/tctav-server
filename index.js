var express = require('express');
var app = express();
var facebookControl = require('./controller/facebook.js'); // Facebook controller

app.set('port', (process.env.PORT || 4001));
app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.get('/test', function(request, response) {

});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

const accessToken = "EAAKrNpt4r2sBAMLmQXa6RDaarqYv87Lak517j3DiAxWsNAXHlW5oxia875cOz8fTTpE4HPm45pzpZB37JqIzU0ZBtTwtYjcCz2de0trSmKkN1hLZBZCinwfZCuFduuKipik0DscFVZAL63IeHrJeoZCDexLdzqwl950XOdwZB2gNxQZDZD";
app.get('/getFBInfo', function(req, res) {
  
});

var replyai_weather = require('./controller/replyai_weather.js');
app.get('/checkCity', function(req, res) {
  console.log("city check : ", req.query.city);
  replyai_weather.checkCity(req.query.city,function callback(result){
    res.send(result);
  });
});
app.get('/checkWeather', function(req, res) {
  console.log("city check : ", req.query.city);
  replyai_weather.checkWeather(req.query.city,function callback(result){
    res.send(result);
  });
});

app.get('/getNearby', function(req, res) {
  console.log(req.query.path);
  replyai.getNearby(req.query.db , req.query.path,function callback(result){
    res.send(result);
  });
});

var replyai = require('./controller/reply_ai_api.js');
app.get('/validateBirthday', function(req, res) {
  console.log("birthday check : ", req.query.birthday);
  replyai.isValidDate(req.query.birthday,function callback(result){
    res.send(result);
  });
});
app.get('/validatePhone', function(req, res) {
  console.log("phone check : ", req.query.phone);
  replyai.isValidPhone(req.query.phone,function callback(result){
    res.send(result);
  });
});
app.get('/getInfor', function(req, res) {
  console.log("Header : " + JSON.stringify(req.headers));
  console.log("Code check : ", req.query.codeapi);
  replyai.getInfoCompany(req.query.codeapi ,function callback(result){
    res.send(result);
  }); 
});
app.get('/getPackageInsurances', function(req, res) {
  replyai.getPackageInsurances(function callback(result){
    res.send(result);
  }); 
});
app.get('/informationPackage', function(req, res) {

  console.log("package check : ", req.query.packagenumber);
  replyai.informationPackage(req.query.packagenumber,function callback(result){
    res.send(result);
  }); 
});
app.get('/getAddress', function(req, res) {
  console.log("Address check : ", req.query.path);
  replyai.getAddress( req.query.path ,function callback(result){
    res.send(result);
  });  
});

app.get('/sendIcal', function(req, res) {
  console.log("email : ", req.query.email);
  var ical = require('ical-generator'),fs= require('fs'),
    http = require('http');
    var eventObj = { 
    	'start' : new Date(), 
    	'end' : new Date(), 
    	'title' : 'Tư vấn bảo hiểm', 
    	'description' : 'Hẹn gặp bạn' ,
    	'id' : 'wdcwe76234e127eugb', //Some unique identifier 
    	'organiser' : {'name' : 'Doan Duy Man', 'email':'ddmantctav02@gmail.com'}, 
    	'location' : 'Goa' 
    }
    var cal = ical();
    cal.setDomain('https://www.prudential.com.vn/').setName('My ical invite'); 
    cal.addEvent({ 
    	start: eventObj.start, 
    	end: eventObj.end, 
    	summary: eventObj.title, 
    	uid: eventObj.id, // Some unique identifier 
    	sequence: 0, 
    	description: eventObj.description, 
    	location: eventObj.location, 
    	organizer: { name: eventObj.organiser.name, email: eventObj.organiser.email }, 
    	method: 'request' 
    }); 
    var path = __dirname + '/'+ eventObj.id + '.ics'; 
    var file = eventObj.id + '.ics';
    cal.saveSync(path); 
    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport({ 
    	service: "Gmail", 
    	auth: { user: "ddmantctav02@gmail.com" , pass: "ujsrlltmceqohltt" } 
    }); 
    fs.readFile("./" + file, function (err, data) {

      var mailObj = { 
      	from: "ddmantctav02@gmail.com", 
      	to: req.query.email, 
      	subject: "TƯ VẤN BẢO HIỂM PRUDENTIAL", 
      	text: "TƯ VẤN BẢO HIỂM PRUDENTIAL",  
      	attachments: [{'filename':  file, 'content': data}]
      	}; 
      
      transporter.sendMail(mailObj, function(err, info){ 
      	console.log(err,info); 
      	if(err == null){
      		var json = { 
              "items": [ 
                {"result": "success"} 
              ] 
            };
      		res.send(json);
      	}else {
      		var json = { 
              "items": [ 
                {"result": "fail"} 
              ] 
            };
      		res.send(json);
      	}
      }); 
    });
    
});
