var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://ttv:ttv@ds133378.mlab.com:33378/prudential';

exports.insert = function(user) {
    var param = [{
        uuid: user.uuid,
        fullname: user.fullname,
        email: user.email,
        phone: user.phone,
        gender: user.gender,
        maritalstatus: user.maritalstatus,
        salary: user.salary
    }];
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        }
        else {
            // Get the documents collection
            var collection = db.collection('users');
            collection.insert(param, {
                w: 1
            }, function(err, result) {
                console.log(err);
                db.close();
            });
        }
    });
};


exports.update = function(user) {
    var param = {
        fullname: user.fullname,
        email: user.email,
        phone: user.phone,
        gender: user.gender,
        maritalstatus: user.maritalstatus,
        salary: user.salary
    };
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        }
        else {
            // Get the documents collection
            var collection = db.collection('users');
            collection.updateOne({
                uuid: user.uuid
            }, {
                $set: param,
                $currentDate: {
                    "lastModified": true
                }
            }, function(err, results) {
                console.log(err);
                db.close;
            });

        }
    });
};

exports.find = function(uuid, callback) {
    console.log('find uuid mongoDB : ', uuid);
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the mongoDB server. Error:', err);
        }
        else {
            // Get the documents collection
            var collection = db.collection('users');

            collection.find({
                uuid: uuid
            }).toArray(function(err, result) {
                console.log(result);
                if (err) {
                    callback(null);
                }
                else if (result.length > 0) {
                    callback(result[0]);
                }
                else {
                    callback(null);
                }
                //Close connection
                db.close();
            });
        }
    });

};