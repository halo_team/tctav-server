exports.isValidDate = function(date,callback)
{
    var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(date);
    if (matches == null) return false;
    var d = matches[1] ;
    var m = matches[2];
    var y = matches[3];
    var composedDate = new Date(y, m, d);

    var json = { 
              "items": [ 
                {"result": composedDate.getDate() + "/" + composedDate.getMonth()+ "/" + composedDate.getFullYear() } 
              ] 
            };
    callback(json);
};


exports.checkCity = function(city, callback) {
    var json;
    var GoogleLocations = require('google-locations');
    console.log("city check : ", city);
    var locations = new GoogleLocations('AIzaSyDO4JQUcM47TWwD3O0mg59GXztJeGcCS7A');
    locations.autocomplete({
        input: city,
        types: "(cities)"
    }, function(err, response) {
        console.log("predictions check city : ", response);
        if (err || response.predictions.length == 0) {
            
            json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
            callback(json);
        }
        else {
            if (response.predictions.length == 1) {
                json = { 
                      "items": [ 
                        {"result": "1"} ,
                        {"name": response.predictions[0].description} 
                      ] 
                    };
                callback(json);
            }
            else {
                json = { 
                      "items": [ 
                        {"result": "2"} ,
                        {"name": response.predictions[0].description} ,
                        {"name": response.predictions[1].description}
                      ] 
                    };
                callback(json);
            }
        }

    });
};

var http = require('http');

exports.checkWeather = function(city , callback) {

    /*var options = {
        host: 'query.yahooapis.com',
        port: 80,
        path: "/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=%27nhatrang%27)&format=json&env=store://datatables.org/alltableswithkeys",
        method: 'GET',
        header: {
            "Content-Type": "application/json"
        }
    };

    var buffer = ''; 
    var request = http.get(options, function(result) {
        result.on('data', function(chunk) {
            //var x = chunk.replace(/\n/g, "");
            buffer += chunk;
        });

        result.on('end', function() {
          callback(buffer);
        });
    });

    request.on('error', function(e) {
        console.log('error from facebook.getFbData: ' + e.message); 
    });

    request.end();*/


    /*var request = require("request");

    var parseMyAwesomeHtml = function(html) {
            console.log(html);
    //Have at it
            var location = html.query.results.channel.location;
            var forecast = html.query.results.channel.item.forecast[0];
            var tempMedium = html.query.results.channel.item.condition.temp;
            var conditionAndUrl = getConditionUrlWeather(forecast.code).split("-and-");
            json = { 
              "items": [ 
                {"result": "1"}  ,
                {
                    "date" : forecast.date,
                    "city" : location.city,
                    "country" : location.country,
                    "highTemp" : forecast.high,
                    "lowTemp" : forecast.low,
                    "condition" : conditionAndUrl[0],
                    "imageUrl" : conditionAndUrl[1],
                    "mediumTemp" : tempMedium
                }
              ] 
            };
            callback(json); 
    };
    console.log("url = " + "https://query.yahooapis.com/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "')&format=json&env=store://datatables.org/alltableswithkeys");
    request({
            url : "http://query.yahooapis.com:80/v1/public/yql?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "')&format=json&env=store://datatables.org/alltableswithkeys",
            json: true
        },
        function (error, response, body) {
        if (!error) {
            console.log(JSON.stringify(body));
            if(body.error != null || body.query.count == "0"){
                var json = { 
                    "items": [ 
                            {"result": "null"} 
                        ] 
                    };
                callback(json);
            }else{
                parseMyAwesomeHtml(body);
            }
            
        } else {
            var json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
            callback(json);
        }
    });*/

    var json;
    var YQL = require('yql');
    var degree = "C";
    var query = new YQL('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' + city + '") and u="' + degree + '"');
    query.exec(function(err, data) {
        console.log('Entries webhoob api:' + JSON.stringify(data));
        if (data == null || data.query.results == null) {
            json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
            callback(json);
        }
        else {
            var location = data.query.results.channel.location;
            var forecast = data.query.results.channel.item.forecast[0];
            var tempMedium = data.query.results.channel.item.condition.temp;
            var conditionAndUrl = getConditionUrlWeather(forecast.code).split("-and-");
            json = { 
              "items": [ 
                {"result": "1"}  ,
                {
                    "date" : forecast.date,
                    "city" : location.city,
                    "country" : location.country,
                    "highTemp" : forecast.high,
                    "lowTemp" : forecast.low,
                    "condition" : conditionAndUrl[0],
                    "imageUrl" : conditionAndUrl[1],
                    "mediumTemp" : tempMedium
                }
              ] 
            };
            callback(json);
            
        }
        console.log(err);
    });
};
function getConditionUrlWeather(code) {
var condition ;
    var baseUrl = "https://s3.amazonaws.com/chatbot-resource/Weather-Condition-Image/"; 
    var imageName = "Breezy.jpg";
    switch (code) {
        case '0':
            condition = "tornado";
            imageName = "hurricane.jpg";
            break;
        case '1':
            condition = "tropical storm";
            imageName = "hurricane.jpg";
            break;
        case '2':
            condition = "hurricane";
            imageName = "hurricane.jpg";
            break;
        case '3':
            condition = "severe thunderstorms";
            imageName = "hurricane.jpg";
            break;
        case '4':
            condition = "thunderstorms";
            imageName = "thunderstorm.jpg";
            break;
        case '5':
            condition = "mixed rain and snow";
            imageName = "heavy_snow.jpg";
            break;
        case '6':
            condition = "mixed rain and sleet";
            imageName = "heavy_snow.jpg";
            break;
        case '7':
            condition = "mixed snow and sleet";
            imageName = "heavy_snow.jpg";
            break;
        case '8':
            condition = "freezing drizzle";
            imageName = "snow.jpg";
            break;
        case '9':
            condition = "drizzle";
            imageName = "snow.jpg";
            break;
        case '10':
            condition = "freezing rain";
            imageName = "Rainy.jpg";
            break;
        case '11':
            condition = "showers";
            imageName = "Rainy.jpg";
            break;
        case '12':
            condition = "showers";
            imageName = "Rainy.jpg";
            break;
        case '13':
            condition = "snow flurries";
            imageName = "snow.jpg";
            break;
        case '14':
            condition = "light snow showers";
            imageName = "snow.jpg";
            break;
        case '15':
            condition = "blowing snow";
            imageName = "snow.jpg";
            break;
        case '16':
            condition = "snow";
            imageName = "snow.jpg";
            break;
        case '17':
            condition = "hail";
            imageName = "rainandhail.jpg";
            break;
        case '18':
            condition = "sleet";
            imageName = "rainandhail.jpg";
            break;
        case '19':
            condition = "dust";
            imageName = "fog.jpg";
            break;
        case '20':
            condition = "foggy";
            imageName = "fog.jpg";
            break;
        case '21':
            condition = "haze";
            imageName = "fog.jpg";
            break;
        case '22':
            condition = "smoky";
            imageName = "fog.jpg";
            break;
        case '23':
            condition = "blustery";
            imageName = "fog.jpg";
            break;
        case '24':
            condition = "windy";
            imageName = "Windy.png";
            break;
        case '25':
            condition = "cold";
            imageName = "cold.jpg";
            break;
        case '26':
            condition = "cloudy";
            imageName = "Cloudy.jpg";
            break;
        case '27':
            condition = "mostly cloudy (night)";
            imageName = "Mostly_Cloudy.jpg";
            break;
        case '28':
            condition = "mostly cloudy (day)";
            imageName = "Mostly_Cloudy.jpg";
            break;
        case '29':
            condition = "partly cloudy (night)";
            imageName = "Partly_Cloudy.jpg";
            break;
        case '30':
            condition = "partly cloudy (day)";
            imageName = "Partly_Cloudy.jpg";
            break;
        case '31':
            condition = "clear (night)";
            imageName = "nice.jpg";
            break;
        case '32':
            condition = "sunny";
            imageName = "Sunny.jpg";
            break;
        case '33':
            condition = "fair (night)";
            imageName = "nice.jpg";
            break;
        case '34':
            condition = "fair (day)";
            imageName = "nice.jpg";
            break;
        case '35':
            condition = "mixed rain and hail";
            imageName = "rainandhail.jpg";
            break;
        case '36':
            condition = "hot";
            imageName = "hot.jpg";
            break;
        case '37':
            condition = "isolated thunderstorms";
            imageName = "thunderstorm.jpg";
            break;
        case '38':
            condition = "scattered thunderstorms";
            imageName = "thunderstorm.jpg";
            break;
        case '39':
            condition = "scattered thunderstorms";
            imageName = "thunderstorm.jpg";
            break;
        case '40':
            condition = "scattered showers";
            imageName = "Rainy.jpg";
            break;
        case '41':
            condition = "heavy snow";
            imageName = "heavy_snow.jpg";
            break;
        case '42':
            condition = "scattered snow showers";
            imageName = "heavy_snow.jpg";
            break;
        case '43':
            condition = "heavy snow";
            imageName = "heavy_snow.jpg";
            break;
        case '44':
            condition = "partly cloudy";
            imageName = "Partly_Cloudy.jpg";
            break;
        case '45':
            condition = "thundershowers";
            imageName = "thunderstorm.jpg";
            break;
        case '46':
            condition = "snow showers";
            imageName = "snow.jpg";
            break;
        case '47':
            condition = "isolated thundershowers";
            imageName = "thunderstorm.jpg";
            break;
        default:
            condition = "not available";
            imageName = "Breezy.jpg";
    }
    return condition + "-and-" + baseUrl + imageName ;
};