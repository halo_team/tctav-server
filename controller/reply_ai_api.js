exports.isValidDate = function(date,callback)
{
    var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(date);
    if (matches == null) {
        var json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
        callback(json);
    }
    var day = parseInt(matches[1], 10);
    var month = parseInt(matches[2], 10);
    var year = parseInt(matches[3], 10);
    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12){
      var json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
      callback(json);
    }
    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;
       
    if(day > 0 && day <= monthLength[month - 1]){
      if(day < 10) day = "0" + day;
      var json = { 
              "items": [ 
                {"result": day + "-" + month + "-" + year } 
              ] 
            };
      callback(json);
    }else {
      var json = { 
              "items": [ 
                {"result": "null"} 
              ] 
            };
      callback(json);
    }
    
    
};
exports.getInfoCompany = function(code ,callback)
{
    if(code == "pdtiontranscosmosman1234"){
        var json = { 
              "company": [ 
                {"result": "1"  ,"title" : "The Prudential Company",
                "image" : "https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/15442231_241092949629405_3344488798433384544_n.jpg?oh=6b1cf89da43add9b9306093ef6044569&oe=58B053AD",
                "item" : "https://www.prudential.com.vn/vi/",
                "subtitle" : "The Prudential Company"}
              ] 
            };
        callback(json);
    }else {
         var json = { 
              "company": [ 
                {"result": "null" } 
              ] 
            };
        callback(json);
    }
};

exports.getPackageInsurances = function(callback)
{
    var json = { 
              "facebook_template":
                  {
                    "type":"template",
                    "payload":
                      {
                        "template_type":"generic",
                        "elements":[
                          { 
                            "title":"UIC Health Care - 4",
                            "image_url":"https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/15442231_241092949629405_3344488798433384544_n.jpg?oh=6b1cf89da43add9b9306093ef6044569&oe=58B053AD",
                            "subtitle":"The Prudential Company",
                            "item_url":"https://www.prudential.com.vn/vi/",
                            "buttons":[
                              {
                                "type": "postback",
                                "title": "Xem chi tiết",
                                "payload": "UIC-Care4"
                              },
                              {
                                "type":"web_url",
                                "url":"https://www.prudential.com.vn/vi/our-company/",
                                "title":"Mở website"
                              },
                              {
                                "type": "postback",
                                "title": "Đặt lịch",
                                "payload": "Đặt lịch"
                              }
                            ]
                          },{ 
                            "title":"UIC Health Care - 1",
                            "image_url":"https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/15442231_241092949629405_3344488798433384544_n.jpg?oh=6b1cf89da43add9b9306093ef6044569&oe=58B053AD",
                            "subtitle":"The Prudential Company",
                            "item_url":"https://www.prudential.com.vn/vi/",
                            "buttons":[
                              {
                                "type": "postback",
                                "title": "Xem chi tiết",
                                "payload": "UIC-Care1"
                              }
                            ]
                          },{ 
                            "title":"UIC Health Care - 2",
                            "image_url":"https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/15442231_241092949629405_3344488798433384544_n.jpg?oh=6b1cf89da43add9b9306093ef6044569&oe=58B053AD",
                            "subtitle":"The Prudential Company",
                            "item_url":"https://www.prudential.com.vn/vi/",
                            "buttons":[
                              {
                                "type": "postback",
                                "title": "Xem chi tiết",
                                "payload": "UIC-Care2"
                              }
                            ]
                          },{ 
                            "title":"UIC Health Care - 3",
                            "image_url":"https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/15442231_241092949629405_3344488798433384544_n.jpg?oh=6b1cf89da43add9b9306093ef6044569&oe=58B053AD",
                            "subtitle":"The Prudential Company",
                            "item_url":"https://www.prudential.com.vn/vi/",
                            "buttons":[
                              {
                                "type": "postback",
                                "title": "Xem chi tiết",
                                "payload": "UIC-Care3"
                              }
                            ]
                          },

                        ]
                      }
                  }
            };
        callback(json);
};

exports.informationPackage = function(txtPackage , callback)
{
    if(txtPackage.indexOf("UIC-Care1") > -1) {
      var json =  { 
                    "package" :[
                      {
                        "result" :"✈  Độ tuổi tham gia : Từ 18 - 55 tuổi\n⚽  Phục vụ nhu cầu : Bảo hiểm tích luỹ cho kế hoạch giáo dục\n🎿 Thời hạn hợp đồng : Kế hoạch nhận định kỳ: 12 - 22 năm\n⛳  Thời hạn đóng phí : Kế hoạch nhận định kỳ: Thời hạn hợp đồng - 4 năm"
                      }
                   ]
                  }
      callback(json);
    }else if(txtPackage.indexOf("UIC-Care2") > -1) {
      var json =  { 
                    "package" :[
                      {
                        "result" :"✈  Độ tuổi tham gia : Từ 6 tháng đến 65 tuổi\n⚽  Phục vụ nhu cầu : Bảo hiểm trước các rủi ro tai nạn và bảo lãnh viện phí 24/7\n🎿 Thời hạn hợp đồng : 05, 10 hoặc 15 năm\n⛳  Thời hạn đóng phí : Bằng thời hạn hợp đồng"
                      }
                   ]
                  }
      callback(json);

    }else if(txtPackage.indexOf("UIC-Care3") > -1) {
      var json =  { 
                    "package" :[
                      {
                        "result" :"Độ tuổi tham gia : Từ 15-60 tuổi\nTuổi tối đa khi kết thúc Hợp đồng : 65 tuổi\nThời hạn hợp đồng : 05 - 30 năm\n⛳  Thời hạn đóng phí : Bằng thời hạn hợp đồng"
                      }
                   ]
                  }
      callback(json);

    }else if(txtPackage.indexOf("UIC-Care4") > -1) {
      var json =  { 
                    "package" :[
                      {
                        "result" :"✈  Độ tuổi tham gia : Nữ: 18 - 54 tuổi, Nam: 18 - 59 tuổi\n⚽  Phục vụ nhu cầu  : Bảo hiểm tích luỹ cho kế hoạch hưu trí\n🎿 Thời hạn hợp đồng : Tính bằng tuổi hưu trừ tuổi tham gia\n⛳  Thời hạn đóng phí : 15 năm"
                      }
                   ]
                  }
      callback(json);

    }else {
      var json = { 
                "package": [ 
                  {"result": "null" } 
                ] 
              };
            callback(json);
    }
}

exports.isValidPhone = function validatePhone(txtPhone , callback)
{
    var phoneDecode = unescape(txtPhone);
    var phone = txtPhone.replace(/[^(0-9| )]+/g,"");
    var arrphone = phone.split(" ");
    for (var i = arrphone.length - 1; i >= 0; i--) {
      if(arrphone[i].length == 10 || arrphone[i].length == 11){
          var matches = /^(099|0199|092|0188|090|093|0121|0122|0124|0126|0128|091|094|0123|0125|0127|0129|097|098|0168|0164|0165|0166|0167|0169).+/.exec(arrphone[i]);
          if (matches != null) {
            var json = { 
                "items": [ 
                  {"result": matches[0] } 
                ] 
              };
            callback(json);
          }
          else {
            var json = { 
                "items": [ 
                  {"result": "null" } 
                ] 
              };
            callback(json);
        }
      }
      if(i==0){
        var json = { 
                "items": [ 
                  {"result": "null" } 
                ] 
              };
            callback(json);
      }
    };
    
};

var NodeGeocoder = require('node-geocoder');
 
var options = {
  provider: 'google',
 
  // Optional depending on the providers 
  httpAdapter: 'https', // Default 
  apiKey: 'AIzaSyDO4JQUcM47TWwD3O0mg59GXztJeGcCS7A', // for Mapquest, OpenCage, Google Premier 
  formatter: 'json'         // 'gpx', 'string', ... 
};
var geocoder = NodeGeocoder(options);
exports.getAddress = function getAdressGoogleMap(path , callback)
{
    var gmaputil = require('googlemapsutil');
    var cb = function(err, data) {
      var jsonResult = JSON.parse(data);

      if(err){
        var json = { 
                "items": [ 
                  {"result": "null" } 
                ] 
              };
            callback(json);
      }else if(jsonResult.status == "ZERO_RESULTS"){
        var json = { 
                "items": [ 
                  {"result": "null" } 
                ] 
              };
            callback(json);
      }else {
        var json = { 
                "items": [ 
                  {
                    "result": jsonResult.results[0].formatted_address,
                    "coordinates" : jsonResult.results[0].geometry.location.lat +"," + jsonResult.results[0].geometry.location.lng
                  } 
                ] 
              };
            callback(json);
      }
    };
    gmaputil.geocoding(encodeURIComponent(path), null, cb);
};

exports.getNearby = function getNearByGoogleMap(fileName , path, callback)
{
  var gmaputil = require('googlemapsutil');
  var StaticMaps = gmaputil.StaticMaps;
      staticmaps = new StaticMaps();
  var cordinate = path.split(",");
  var arrJsonCoordinates = [];
  var fileJson ;
  var cordinatePosition;
  var position = 0 ;
  var checkMin = 0 ;
  // call api from class object
  var staticMap = function(err, result) {
    if (err) {
      console.log(err);
    }
    var item = "https://www.google.com.vn/maps/dir/"+cordinate[0]+","+cordinate[1]+"/" +cordinatePosition;
    var json = { 
              "address": [ 
                {
                  "title" : fileJson[position].Title,
                  "image" : result,
                  "item" : item,
                  "subtitle" : fileJson[position].Subtitle
                }
              ] 
            };
    callback(json);
  };
  var cb = function(err, result) {
    //callback(result);
    if (err) {
      console.log(err);
    }else {
      var jsonResult = JSON.parse(result);

      // Check if jsonResult has distance value
      var tmp;
      if (jsonResult.hasOwnProperty('rows')) {
          tmp = jsonResult.rows[0];
          if (tmp.hasOwnProperty('elements')) {
              tmp = tmp.elements[0];
              if (tmp.hasOwnProperty('distance')) {
                var min = tmp.distance.value;
                for (var i = 0; i < jsonResult.rows.length; i++) {
                    checkMin = jsonResult.rows[i].elements[0].distance.value ;
                    if(checkMin < min){
                      min = checkMin ;
                      position = i;
                    }
                };
                console.log(position);
                cordinatePosition = fileJson[position].Coordinates.lat +',' + fileJson[position].Coordinates.lng;
                staticmaps.staticmap({center: cordinatePosition, zoom: 17, size: '400x200', markers : 'color:red%7Clabel:S%7C'+ cordinatePosition}, staticMap, null, null, false);
              }
          }
      }       
    }
  };
  var callbackReadFile = function(err, result) {
    fileJson = result;
    for (var i = 0 ; i < result.length ; i++) {
      var xlat = result[i].Coordinates.lat;
      var xlng = result[i].Coordinates.lng;
      var cordinateFile = {lat: xlat,lng: xlng};
      arrJsonCoordinates.push(cordinateFile);
    };
    gmaputil.distancematrix(arrJsonCoordinates,[{lat:cordinate[0], lng:cordinate[1]}],null, cb); 
  };
  readFile(fileName , callbackReadFile);
  
};

function readFile(fileName , callbackReadFile){
  var fs = require('fs');
  fs.readFile(fileName+'.json', 'utf8', function (err,data) {
    if (err) {
      console.log(err);
    }
    callbackReadFile(err,JSON.parse(data));
  });
}