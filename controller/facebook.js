var request = require("request");
/*
 * Send text message from ChatBot to User
 * @param: recipientId, messageText
 * @return: none
 */
exports.sendTextMessage = function(recipientId, messageText) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      text: messageText
    }
  };
  callSendMessageAPI(messageData);
};
/*
 * Call send API of messages (url: https://graph.facebook.com/v2.6/me/messages)
 * @param: messageData
 * @return: none
 */
function callSendMessageAPI(messageData) {

  callSendAPI('https://graph.facebook.com/v2.6/me/messages', messageData);
}
/*
 * Call send API by url string
 * Author: Pham Chi Cong
 * @param: urlString, messageData
 * @return: none
 */
function callSendAPI(urlString, messageData) {

  request({
    uri: urlString,
    qs: {
      access_token: "EAAYsZBOASiZBQBAPzS9fvYd9K2nTwlkSWvnYEJTNQVGo3V5ZBE8cbzMe5UUfMkjR9YsYf3eoAe16JvJo2Ru4cy7yNzmQbKNIIvLsHl4zEvRo23RzPq8Epu5gxIcC82sSqvVuoSG7qlZB2bfbRXQjYrVv0z93q3yXzd9uAziDqgZDZD"
    },
    method: 'POST',
    json: messageData
  }, function(error, response, body) {
    if (!error && response.statusCode == 200) {

      var recipientId = body.recipient_id;
      var messageId = body.message_id;
      console.log("Successfully sent generic message with id %s to recipient %s", messageId, recipientId);
    }
    else {
      console.error(error);
    }
  });
}